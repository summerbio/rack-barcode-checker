/**************************************************************************************************
AUTHOR: 				Jeffrey Diament
DATE LAST MODIFIED: 	3/16/2021
DESCRIPTION:
RackBarcodeChecker.js contains a code that allows operators to manually scan rack barcodes are incorrectly scanned initially
RackBarcodeChecker is a singleton used in RNA Source swimlanes of SB3 production protocols
it is re-initialized (all non-constant properties set to null) before it's used in each swimlane
***************************************************************************************************/
var RackBarcodeChecker = {
    
    // properties specific to a given "state" of the RackBarcodeChecker object
    newRackBarcode         	: null, // 1D rack barcode
    badBarcodeFlag      	: null, // null: no info about rack barcode; true: invalid barcode; false: valid barcode

    /*************************************************************************************************** 	
	DESCRIPTION: resets state of singleton by setting all properties to null
   	INPUTS: none
   	OUTPUTS: none  
	ERROR CHECKING: none
	***************************************************************************************************/     
	resetState          : function() {
        this.newRackBarcode 		= null
        this.badBarcodeFlag     	= null
        //plate.setBarcode(WEST, "")

        print("RackBarcodeChecker.resetState(): state is reset")
    },

    /*************************************************************************************************** 	
	DESCRIPTION: checks if current barcode is good (8 digits long)
   	INPUTS: none
   	OUTPUTS: none  
	ERROR CHECKING: none
	***************************************************************************************************/     
	checkBarcode          : function() {
		if (plate.barcode[WEST] != null) {
			if (plate.barcode[WEST].length == 8) {
				this.badBarcodeFlag = false
		        print("RackBarcodeChecker.checkBarcode(): good barcode (8 digits): " + plate.barcode[WEST])
			}
			else {
				this.badBarcodeFlag = true
		        print("RackBarcodeChecker.checkBarcode(): bad barcode (NOT 8 digits): " + plate.barcode[WEST])
			}
		}
		else {
			this.badBarcodeFlag = true
	        print("RackBarcodeChecker.checkBarcode(): null barcode")
		}
    },

    /*************************************************************************************************** 	
	DESCRIPTION: if rackBarcode is not null set plate.barcode[WEST] --> this.rackBarcode  
   	INPUTS: none
   	OUTPUTS: none (only print statements to VWorks log) 
	ERROR CHECKING: print error messages to log and do not execute function if: 
	rackBarcode (function input)  	= null
	***************************************************************************************************/  
    setRackBarcode       : function (rackBarcode) {
        if (rackBarcode != null) {
            plate.setBarcode(WEST, String(rackBarcode))
            print("RackBarcodeChecker.setRackBarcode(): new rack barcode is: " + rackBarcode)    
        }     
        else {print("TubeBarcodeChecker.setRackBarcode(): rackBarcode is null")} 
    },

    /***************************************************************************************************	
	DESCRIPTION: output string showing current state of the singleton -- this can be printed to VWorks log
   	INPUTS: none
   	OUTPUTS: STRING  
	ERROR CHECKING: none
	***************************************************************************************************/     
	printState          : function() {
        var returnString = ""
        + "rackBarcode: " 					+ this.rackBarcode  	+ "\n" 
        + "badBarcodeFlag: " 				+ this.badBarcodeFlag 	+ "\n"

        return returnString
    }
};